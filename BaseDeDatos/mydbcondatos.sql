-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-03-2022 a las 22:03:31
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `mydb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(45) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `Nombre`, `Telefono`, `Correo`, `Clave`) VALUES
(1, 'jose', '3504769382', 'jose@gmail.com', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `idcarrito` int(11) NOT NULL,
  `Producto_idProducto` int(11) NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Direccion` varchar(45) NOT NULL,
  `Telefono` varchar(30) DEFAULT NULL,
  `Correo` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `Nombre`, `Direccion`, `Telefono`, `Correo`, `Clave`) VALUES
(1, 'Maria Alejandra Cabezas', 'Dig 15B #104-45', '301 3390100', 'MalejaC@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b'),
(3, 'Andrea romero Bolaños', 'Cra 66 #46-84', '312 4540127', 'AngreaRB@gmail.com', '182be0c5cdcd5072bb1864cdee4d3d6e'),
(4, 'Andrés Esteban Cadavid', 'Cra 23 #142-80', '301 5529046', 'EsCadavid@gmail.com', 'a87ff679a2f3e71d9181a67b7542122c'),
(5, 'Lina Marcela Suarez', 'Dg 73a sur #78i50 ', '314 556 0937', 'SuarezLina@gmail.com', 'e4da3b7fbbce2345d7772b0674a318d5'),
(6, 'José Armando Casas', ' calle 12 c #71c-30', '312 4539328', 'armandoCasas@gmail.com', '1679091c5a880faf6fb5e6087eb1b2dc'),
(1000225544, 'jose perez', 'calle 31', '1478', 'prueba@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `idCompra` int(11) NOT NULL,
  `fecha_compra` datetime NOT NULL,
  `Cliente_idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`idCompra`, `fecha_compra`, `Cliente_idCliente`) VALUES
(2275581, '2022-03-17 14:33:42', 4),
(2652112, '2022-03-17 14:35:55', 1),
(4050343, '2022-03-17 15:52:20', 1),
(7221514, '2022-03-17 14:35:00', 4),
(8285388, '2022-03-17 15:02:36', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(45) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Clave` varchar(45) NOT NULL,
  `Administrador_idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `Nombre`, `Telefono`, `Correo`, `Clave`, `Administrador_idAdministrador`) VALUES
(1, 'Juan Felipe Vargas', '313 2875642', 'JuanFelipeV@gmail.com', 'c4ca4238a0b923820dcc509a6f75849b', 1),
(2, 'Sebastian Tolosa', '301 4637890', 'ToloSebastian@gmail.com', 'c81e728d9d4c2f636f067f89cc14862c', 1),
(3, 'Andrés Felipe Aguilar', '350 4789324', 'AguilarFelipe@gmail.com', 'eccbc87e4b5ce2fe28308fd9f2a7baf3', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE `pedido` (
  `idPedido` int(11) NOT NULL,
  `Compra_idCompra` int(11) NOT NULL,
  `Compra_Cliente_idCliente` int(11) NOT NULL,
  `Empleado_idEmpleado` int(11) NOT NULL,
  `Producto_idProducto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`idPedido`, `Compra_idCompra`, `Compra_Cliente_idCliente`, `Empleado_idEmpleado`, `Producto_idProducto`) VALUES
(4728268, 2275581, 4, 3, 1),
(8011739, 7221514, 4, 2, 1),
(8946433, 8285388, 1, 3, 1),
(9415473, 2652112, 1, 2, 1),
(9587258, 4050343, 1, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `precio` double NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `imagen` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `Nombre`, `precio`, `descripcion`, `imagen`) VALUES
(1, 'Pizza Personal Hawaiana ', 4500, 'Porción de pizza hawaiana personal con borde de bocadillo y queso Chedar derretido', 'img/pizza_hawa_peque.jpg'),
(2, 'Pizza personal de pepperoni ', 5500, 'Porción de pizza de pepperoni personal con borde de bocadillo y queso Brie derretido', 'img/pizza_peppe_peque.jpg'),
(3, 'Pizza personal de Carnes', 5000, 'Porción de pizza de carnes personal con borde de bocadillo y queso camembert derretido', 'img/pizza_carne_pequeña.jpg'),
(4, 'Pizza familiar Hawaiana', 33900, 'Pizza hawaiana tamaño familiar (8 porciones), borde de bocadillo, con queso, piña, mozzarella y jamón. Ingredientes de la mejor calidad', 'img/pizza_hawa_Fa.jpg'),
(5, 'Pizza familiar de pepperoni', 36500, 'Pizza de pepperoni tamaño familiar (8 porciones), borde de bocadillo, queso brie derretido, pepperoni, mozzarella y jamón. Ingredientes de la mejor calidad', 'img/pizza_peppe_Fa.jpg'),
(6, 'Pizza familiar de carnes', 34800, 'Pizza de carnes tamaño familiar (8 porciones), borde de bocadillo, con queso, carne molida, chorizo, pepperoni, jamón y tocino. Ingredientes de la mejor calidad', 'img/pizza_carnes_Fa.jpg'),
(7, 'Gaseosa personal 350ml', 2000, 'Gaseosa personal manzana Postobón- 350ml', 'img/gaseosa_postobon_350.jpg'),
(8, 'Gaseosa 1000ml', 3200, 'Gaseosa Coca cola 1.0L (5-6 personas)', 'img/gaseosa_cola_1.5.jpg'),
(9, 'Gaseosa grande familiar 2.5L', 5000, 'Gaseosa Coca cola grande 2.5L (8-9 personas)', 'img/gaseosa_cola_5.jpg'),
(10, 'Combo personal Hawaiana', 6000, 'Incluye porción de pizza hawaiana personal + una gaseosa Postobón personal (350ml)', 'img/combo_peque_gaseo_hawa.jpg'),
(11, 'Combo personal pepperoni', 7000, 'Incluye porción de pizza de pepperoni personal + una gaseosa Postobón personal (350ml)', 'img/combo_peque_gaseo_peppe.jpg'),
(12, 'Combo personal pizza de carnes', 6500, 'Incluye porción de pizza de carnes personal + una gaseosa Postobón personal (350ml)', 'img/combo_peque_gaseo_carnes.jpg'),
(13, 'Combo familiar hawaiano', 38000, 'Incluye una pizza grande de 8 porciones hawaiana + 1 gaseosa Coca cola grande (2.5L)', 'img/combo_fa_gaseo_hawa.jpg'),
(14, 'Combo familiar pepperoni', 41000, 'Incluye una pizza grande de 8 porciones de pepperoni + 1 gaseosa Coca cola grande (2.5L)', 'img/combo_fa_gaseo_peppe.jpg'),
(15, 'Combo familiar pizza de carnes', 39000, 'Incluye una pizza grande de 8 porciones de carnes + 1 gaseosa Coca cola grande (2.5L)', 'img/combo_fa_gaseo_carnes.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`idcarrito`,`Producto_idProducto`,`Cliente_idCliente`),
  ADD KEY `fk_carrito_Producto1_idx` (`Producto_idProducto`),
  ADD KEY `fk_carrito_Cliente1_idx` (`Cliente_idCliente`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`idCompra`,`Cliente_idCliente`),
  ADD KEY `fk_CompraVenta_Cliente1_idx` (`Cliente_idCliente`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`,`Administrador_idAdministrador`),
  ADD KEY `fk_Empleado_Administrador1_idx` (`Administrador_idAdministrador`);

--
-- Indices de la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD PRIMARY KEY (`idPedido`,`Compra_idCompra`,`Compra_Cliente_idCliente`,`Empleado_idEmpleado`,`Producto_idProducto`),
  ADD KEY `fk_Pedido_Compra1_idx` (`Compra_idCompra`,`Compra_Cliente_idCliente`),
  ADD KEY `fk_Pedido_Empleado1_idx` (`Empleado_idEmpleado`),
  ADD KEY `fk_Pedido_Producto1_idx` (`Producto_idProducto`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `idcarrito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT de la tabla `pedido`
--
ALTER TABLE `pedido`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10000008;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `fk_carrito_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_carrito_Producto1` FOREIGN KEY (`Producto_idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `fk_CompraVenta_Cliente1` FOREIGN KEY (`Cliente_idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `fk_Empleado_Administrador1` FOREIGN KEY (`Administrador_idAdministrador`) REFERENCES `administrador` (`idAdministrador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pedido`
--
ALTER TABLE `pedido`
  ADD CONSTRAINT `fk_Pedido_Compra1` FOREIGN KEY (`Compra_idCompra`,`Compra_Cliente_idCliente`) REFERENCES `compra` (`idCompra`, `Cliente_idCliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pedido_Empleado1` FOREIGN KEY (`Empleado_idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Pedido_Producto1` FOREIGN KEY (`Producto_idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
