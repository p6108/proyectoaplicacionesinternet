<?php
require_once 'modelo/persistencia/Conexion.php';
require_once 'modelo/persistencia/AdministradorDAO.php';
class Administrador{
    private $id;
    private $nombre;
    private $telefono;
    private $correo;
    private $clave;
    private $conexion;
    private $administradorDAO;
    
	public function getClave()
	{
		return $this->clave;
	}



	public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

	public function getTelefono()
	{
		return $this->telefono;
	}

	public function getCorreo()
    {
        return $this->correo;
    }

    
    public function __construct($id="", $nombre="",  $telefono="", $correo="", $clave=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> telefono = $telefono;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> conexion = new Conexion();
        $this -> administradorDAO = new AdministradorDAO($this -> id, $this -> nombre, $this -> telefono, $this -> correo, $this -> clave);
      
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
       
     	$this -> conexion -> ejecutar($this -> administradorDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $this -> id = $this -> conexion -> extraer()[0];
            return true;
        }else{
            return false;
        }                
    }
    

    public function consultar(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> administradorDAO -> consultar());
    	$this -> conexion -> cerrar();
    	$datos = $this -> conexion -> extraer();
    	$this -> nombre = $datos[0];
    	$this -> telefono = $datos[1];
    	$this -> correo = $datos[2];
 
    }
    
    public function CambiarDatos(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> administradorDAO -> Cambiar());
    	$this -> conexion -> cerrar();
    }
    
    public function CambiarClave(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> administradorDAO -> CambiarClave());
    	$this -> conexion -> cerrar();
    }
    
}

?>
