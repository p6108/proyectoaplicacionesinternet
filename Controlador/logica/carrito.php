<?php
//require_once 'Controlador/logica/producto.php';
//require_once 'Controlador/logica/cliente.php';
//require_once 'Controlador/logica/Administrador.php';
require_once 'modelo/persistencia/Conexion.php';
require_once 'modelo/persistencia/carritoDAO.php';

class carrito{
	private $id;
	private $idCliente;
	private $idProducto;
	private $conexion;
	private $carritoDAO;
	private $cant;

	public function getId()
	{
		return $this->id;
	}

	public function getIdCliente()
	{
		return $this->idCliente;
	}

	public function getIdProducto()
	{
		return $this->idProducto;
	}

	public function getCant()
	{
		return $this->cant;
	}

	public function __construct($id = "",$idCliente = "",$idProducto = "",$cant="")
	{
		//echo "Entró al contructor".$id."".$idProducto."".$idCliente."";

		$this -> id = $id;
		$this -> idCliente = $idCliente;
		$this -> idProducto = $idProducto;
		$this -> cant =  $cant;
		$this -> conexion = new Conexion();
		$this -> carritoDAO = new carritoDAO($this ->idCliente, $this ->idProducto, $this-> cant);
	}

	public function agregarCarro(){
		$this ->conexion ->abrir();
		//echo "\t".$this ->carritoDAO->crearCarrito()."";
		$this ->conexion ->ejecutar($this ->carritoDAO-> crearCarrito() );
		echo '<script language="javascript">alert("Producto añadido :D");</script>';
		$this ->conexion ->cerrar();
	}

	public function verCarrito(){
		$this ->conexion ->abrir();
		//echo $this ->carritoDAO->traerCarro();	
		$this ->conexion ->ejecutar($this ->carritoDAO->traerCarro());
		$productos = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$producto = new producto($registro[0]);
			$producto = $producto -> traerProductoId();
			array_push($productos, $producto);
		}
		$this ->conexion ->Cerrar();
		return $productos;
	}
	
	public function traerProducto(){
		$this ->conexion ->abrir();
		//echo $this ->carritoDAO->traerCarro();
		$this ->conexion ->ejecutar($this ->carritoDAO->traerCarro());
		$productosid = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$producto = new producto($registro[0]);
			$producto = $producto -> traerProductoId();
			array_push($productosid, $producto);
		}
		$this ->conexion ->Cerrar();
		return $productosid;
	}
	
	public function traerP(){
		$this ->conexion ->abrir();
		//echo $this ->carritoDAO->traerCarro();
		$this ->conexion ->ejecutar($this ->carritoDAO->traerProducto());
		$productosid = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$producto = new producto($registro[0]);
			$producto = $producto -> traerProducto();
			array_push($productosid, $producto);
		}
		$this ->conexion ->Cerrar();
		return $productosid;
	}
	
	public function traerCant(){
		$this ->conexion ->abrir();
		//echo "\t".$this ->carritoDAO->crearCarrito()."";
		$this ->conexion ->ejecutar($this ->carritoDAO-> traercant() );
		$cantidades = array();
		$cont=0;
		while(($registro = $this -> conexion -> extraer()) != null){
			//print_r($registro);
			$cantidad = $registro[$cont];
			array_push($cantidades, $cantidad);
		}
		$this ->conexion ->cerrar();
		return $cantidades;
	}
	
	
}


?>
