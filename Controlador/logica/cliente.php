<?php
require_once 'modelo/persistencia/Conexion.php';
require_once 'modelo/persistencia/clienteDAO.php';
class cliente{
	private $id;
	private $nombre;
	private $direccion;
	private $telefono;
	private $correo;
	private $clave;
	private $conexion;
	private $clienteDAO;
	/**
	 * @return Conexion
	 */
	public function getConexion()
	{
		return $this->conexion;
	}

	/**
	 * @return clienteDAO
	 */
	public function getClienteDAO()
	{
		return $this->clienteDAO;
	}

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @param Ambigous <string, unknown> $nombre
	 */
	public function setNombre($nombre)
	{
		$this->nombre = $nombre;
	}

	/**
	 * @param string $direccion
	 */
	public function setDireccion($direccion)
	{
		$this->direccion = $direccion;
	}

	/**
	 * @param string $telefono
	 */
	public function setTelefono($telefono)
	{
		$this->telefono = $telefono;
	}

	/**
	 * @param Ambigous <string, unknown> $correo
	 */
	public function setCorreo($correo)
	{
		$this->correo = $correo;
	}

	/**
	 * @param string $clave
	 */
	public function setClave($clave)
	{
		$this->clave = $clave;
	}

	/**
	 * @param Conexion $conexion
	 */
	public function setConexion($conexion)
	{
		$this->conexion = $conexion;
	}

	/**
	 * @param clienteDAO $clienteDAO
	 */
	public function setClienteDAO($clienteDAO)
	{
		$this->clienteDAO = $clienteDAO;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	public function getTelefono(){
		return $this -> telefono;
	}
	/**
	 * @return mixed
	 */
	public function getNombre()
	{
		return $this->nombre;
	}

	/**
	 * @return mixed
	 */
	public function getDireccion()
	{
		return $this->direccion;
	}

	/**
	 * @return mixed
	 */
	public function getCorreo()
	{
		return $this->correo;
	}

	/**
	 * @return mixed
	 */
	public function getClave()
	{
		return $this->clave;
	}
	
	
	public function __construct($id="", $nombre="", $direccion="",$telefono="", $correo="", $clave=""){
		$this -> id = $id;
		$this -> nombre = $nombre;
		$this -> direccion = $direccion;
		$this -> telefono = $telefono;
		$this -> correo = $correo;
		$this -> clave = $clave;
		$this -> conexion = new Conexion();
		$this -> clienteDAO = new clienteDAO($this -> id, $this -> nombre, $this -> direccion,$this -> telefono ,$this -> correo, $this -> clave);
	}
	
	
	public function crear(){
		$this -> conexion -> abrir();
		
		$this -> conexion -> ejecutar($this -> clienteDAO -> ingresar());
		
		$this -> conexion -> cerrar();
	}
	
	public function autenticar(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
		$this -> conexion -> cerrar();
		if($this -> conexion -> numFilas() == 1){
			$this -> id = $this -> conexion -> extraer()[0];
			return true;
		}else{
			return false;
		}
	}
	
	public function consultar(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
		$datos = $this -> conexion -> extraer();
		$this -> nombre = $datos[0];
		$this -> direccion = $datos[1];
		$this -> telefono = $datos[2];
		$this -> correo = $datos[3];
		$this -> conexion -> cerrar();
	}
	
	public function CambiarDatos(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> clienteDAO -> Cambiar());
		$this -> conexion -> cerrar();
	}
	
	public function CambiarClave(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> clienteDAO -> CambiarClave());
		$this -> conexion -> cerrar();
	}
	
	public function consultarTodo(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> clienteDAO -> consultarTodos());
		$clientes = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$cliente = new cliente($registro[0], $registro[1], $registro[2], $registro[3],$registro[4], $registro[5]);
			array_push($clientes, $cliente);
		}
		$this -> conexion -> cerrar();
		return $clientes;
	}
	public function consultar2(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
		$datos = $this -> conexion -> extraer();
		$this -> nombre = $datos[0];
		$this -> direccion = $datos[1];
		$this -> telefono = $datos[2];
		$this -> conexion -> cerrar();
		return $datos;
	}
	
	
}

?>