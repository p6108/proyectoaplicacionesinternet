<?php
    require_once 'modelo/persistencia/Conexion.php';
    require_once 'modelo/persistencia/domDAO.php';
    
class dom{

    private $id;
    private $Nombre;
    private $Telefono;
    private $Correo;
    private $Clave;
    private $conexion;
    private $domDAO;

    public function getId()
	{
		return $this->id;
	}
	public function getNombre()
	{
		return $this->Nombre;
	}
	public function getTelefono()
	{
		return $this->Telefono;
	}
	public function getCorreo()
	{
		return $this->Correo;
	}
	public function getClave()
	{
		return $this->Clave;
	}
	public function getConexion()
	{
		return $this->conexion;
	}
	public function getDomDAO()
	{
		return $this->domDAO;
	}

    
    public function __construct($id = "", $Nombre = "", $Telefono ="",$Correo ="", $Clave=""){
        $this ->id = $id;
        $this ->Nombre = $Nombre;
        $this ->Telefono = $Telefono;
        $this ->Correo = $Correo;
        $this ->Clave = $Clave;
        $this ->conexion = new Conexion();
        $this -> domDAO = new domDAO($this ->id, $this ->Nombre, $this ->Telefono, $this ->Correo, $this ->Clave);
    }
    
    
    public function autenticar(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> domDAO -> autenticar());
		$this -> conexion -> cerrar();
		if($this -> conexion -> numFilas() == 1){
			$this -> id = $this -> conexion -> extraer()[0];
			return true;
		}else{
			return false;
		}
	}

    public function consultar(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> domDAO -> consultar());
		$this -> conexion -> cerrar();
		$datos = $this -> conexion -> extraer();
		$this -> Nombre = $datos[0];
		$this -> Telefono = $datos[1];
		$this -> Correo = $datos[2];
		
	}
	
	public function CambiarDatos(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> domDAO -> Cambiar());
		$this -> conexion -> cerrar();
	}
	
	public function CambiarClave(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> domDAO -> CambiarClave());
		$this -> conexion -> cerrar();
	}
}

?>