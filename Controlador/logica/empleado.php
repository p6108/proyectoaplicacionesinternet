<?php
    require_once 'modelo/persistencia/Conexion.php';
    require_once 'modelo/persistencia/empleadoDAO.php';
class empleado{
    private $id;
    private $nombre;
    private $telefono;
    private $correo;
    private $clave;
    private $conexion;
    private $empleadoDAO;

    public function getId(){
        return $this -> id;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getIdTelefono(){
        return $this -> telefono;
    }

    public function getCorreo(){
        return $this -> correo;
    }

    public function getClave(){
        return $this -> clave;
    }

    public function __construct($id="", $nombre="", $telefono="",$correo="",$clave="")
    {
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> telefono = $telefono;
        $this -> correo = $correo;
        $this -> clave = $clave;
        $this -> conexion = new Conexion();
        $this -> empleadoDAO = new empleadoDAO($this -> id, $this -> nombre, $this -> telefono, $this -> correo, $this -> clave);
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        //echo $this -> administradorDAO -> autenticar();
     	$this -> conexion -> ejecutar($this -> empleadoDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $this -> id = $this -> conexion -> extraer()[0];
            return true;
        }else{
            return false;
        }                
    }
    
    public function CambiarDatos(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> empleadoDAO -> Cambiar());
    	$this -> conexion -> cerrar();
    }
    public function CambiarClave(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> empleadoDAO -> CambiarClave());
    	$this -> conexion -> cerrar();
    }
    
    public function consultar(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> empleadoDAO -> consultar());
    	$this -> conexion -> cerrar();
    	$datos = $this -> conexion -> extraer();
    	$this -> nombre = $datos[0];
    	$this -> telefono = $datos[1];
    	$this -> correo = $datos[2];
    
    }
  
    public function consultarDatos(){
    	$this -> conexion -> abrir();
    	$this -> conexion -> ejecutar($this -> empleadoDAO -> consultarDatos());
    	$empleados = array();
    	while(($registro = $this -> conexion -> extraer()) != null){
    		$empleado = new Administrador($registro[0], $registro[1], $registro[2], $registro[3], $registro[4]);
    		array_push($empleados,$empleado);
    	}
    	$this -> conexion -> cerrar();
    	return  $empleados;
    }
    
}
?>