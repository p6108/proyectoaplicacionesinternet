<?php
require_once 'modelo/persistencia/Conexion.php';
require_once 'modelo/persistencia/historialDAO.php';


class historial{
	private $id;
	private $fecha;
	private $idProducto;
	private $NombreProducto;
	private $idCompra;
	private $idCliente;
    private $conexion;
	private $cantidad;
	private $precio;
	private $subtotal;
	private $total;
	private $idEmpleado;
	
	
	/**
	 * @return mixed
	 */
	public function getIdEmpleado()
	{
		return $this->idEmpleado;
	}

	public function getprecio()
	{
		return $this->precio;
	}

	public function getsubtotal()
	{
		return $this->subtotal;
	}

	public function getcantidad()
	{
		return $this->cantidad;
	}
	
	public function getId()
	{
		return $this->id;
	}

	public function getIdCliente()
	{
		return $this->idCliente;
	}

	public function getIdProducto()
	{
		return $this->idProducto;
	}

    public function getFecha()
	{
		return $this->fecha;
	}

    public function getIdCompra()
	{
		return $this->idCompra;
	}

	public function getNombreProducto()
	{
		return $this->NombreProducto;
	}

	public function getTotal()
	{
		return $this->total;
	}

	public function __construct($id = "", $fecha= "",$idProducto = "",$NombreProducto="", $idCompra = "",$idCliente = "", $cantidad = "", $precio ="", $subtotal = "", $total = "",$idEmpleado="")
	{
		//echo "Entró al contructor".$id."".$idProducto."".$idCliente."";

		$this -> id = $id;
		$this -> idCliente = $idCliente;
		$this -> idProducto = $idProducto;
		$this -> NombreProducto = $NombreProducto;
        $this -> fecha = $fecha;
        $this -> idCompra = $idCompra;
		$this -> cantidad = $cantidad;
		$this -> precio = $precio;
		$this -> subtotal = $subtotal;
		$this -> total = $total;
		$this -> idEmpleado = $idEmpleado;
		$this -> conexion = new Conexion();
		$this -> historialDAO = new historialDAO($this -> id,$this -> fecha, $this -> idProducto,$this -> NombreProducto, $this -> idCompra, $this -> idCliente, $this->cantidad,$this->precio, $this->subtotal,$this -> total,$this -> idEmpleado);
	}

	public function agregarHistorial(){
		$this ->conexion ->abrir();
        //echo $this ->historialDAO->crearHistorial();
		$this ->conexion ->ejecutar($this ->historialDAO->crearHistorial());
		$this ->conexion ->cerrar();
	}

	public function verHistorial(){
		$this -> conexion -> abrir();
		//echo $this -> historialDAO -> verHistorial();
		$this -> conexion -> ejecutar($this -> historialDAO -> verHistorial());
		$historiales = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$historial = new historial($registro[0], $registro[1], $registro[2],$registro[3],$registro[4], $registro[5], $registro[6], $registro[7], $registro[8], $registro[9],$registro[10]);
			array_push($historiales, $historial);
		}
		$this -> conexion -> cerrar();
		return  $historiales;
	}

	
	
	public function verHistorialEmpleado(){
		$this -> conexion -> abrir();
		//echo $this -> historialDAO -> verHistorialEmpleado();
		$this -> conexion -> ejecutar($this -> historialDAO -> verHistorialEmpleado());
		$historiales = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$historial = new historial($registro[0], $registro[1], $registro[2],$registro[3],$registro[4], $registro[5], $registro[6], $registro[7], $registro[8], $registro[9],$registro[10]);
			array_push($historiales, $historial);
		}
		$this -> conexion -> cerrar();
		return  $historiales;
	}

	public function verHistorial2(){
		$this -> conexion -> abrir();
		//echo $this -> historialDAO -> verHistorial();
		$this -> conexion -> ejecutar($this -> historialDAO -> verHistorial2());
		$historiales = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$historial = new historial($registro[0], $registro[1], $registro[2],$registro[3],$registro[4], $registro[5], $registro[6], $registro[7], $registro[8], $registro[9],$registro[10]);
			array_push($historiales, $historial);
		}
		$this -> conexion -> cerrar();
		return  $historiales;
	}

	public function traerMAX($id){
		$this ->conexion ->abrir();
		//echo "\t".$this ->carritoDAO->crearCarrito()."";
		$this ->conexion ->ejecutar($this ->historialDAO-> traerMAX($id));
		$MAX = array();
		$cont=0;
		while(($registro = $this -> conexion -> extraer()) != null){
			//print_r($registro);
			$cantidad = $registro[$cont];
			array_push($MAX, $cantidad);
		}
		$this ->conexion ->cerrar();
		return $MAX;
	}

	
	public function verHistorial3($idc){
		$this -> conexion -> abrir();
		
		$this -> conexion -> ejecutar($this -> historialDAO -> verHistorial3($idc));
		
		$fila = $this -> conexion ->extraer();
		$this -> conexion -> cerrar();
		//echo $this -> historialDAO -> verHistorial3($idc)."<br>";
		return $fila;
		//$historial = new historial($registro = $this ->conexion->extraer());
		/*$historiales = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$historial = new historial($registro[0], $registro[1], $registro[2],$registro[3],$registro[4], $registro[5], $registro[6], $registro[7], $registro[8], $registro[9],$registro[10]);
			array_push($historiales, $historial);
		}
		$this -> conexion -> cerrar();
		return  $historiales;*/
	}
	
	public function grafica(){
		$this -> conexion -> abrir();
		
		$this -> conexion -> ejecutar($this -> historialDAO -> grafica());
		
		$historiales = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			array_push($historiales, $registro);
		}
		
		
		$this -> conexion -> cerrar();
		
		return $historiales;
	}
	
	public function grafica2(){
		$this -> conexion -> abrir();
		
		$this -> conexion -> ejecutar($this -> historialDAO -> grafica2());
		
		$historiales = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			array_push($historiales, $registro);
		}
		
		
		$this -> conexion -> cerrar();
		
		return $historiales;
	}
	
}

?>
