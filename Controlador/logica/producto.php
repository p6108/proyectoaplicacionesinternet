<?php

require_once 'modelo/persistencia/Conexion.php';
require_once 'modelo/persistencia/productoDAO.php';

class producto{
	private $id;
	private $nombre;
	private $precio;
	private $descripcion;
	private $imagen;
	private $productoDAO;
	private $conexion;
	
	/**
	 * @return string
	 */

	/*public function getKey(){
		return $this->key;
	}

	public function getCod(){
		return $this->COD;
	}*/

	public function getImagen()
	{
		return $this->imagen;
	}

	public function getId(){
		return $this ->id;
	}
	
	public function getNombre(){
		return $this ->nombre;
	}
	
	public function getPrecio(){
		return $this ->precio;
	}
	
	public function getDescripcion(){
		return $this ->descripcion;
	}
	
	
	public function __construct($id="",$nombre="",$precio="",$descripcion="",$imagen=""){
		$this -> id = $id;
		$this -> nombre = $nombre;
		$this -> precio = $precio;
		$this -> descripcion = $descripcion;
		$this -> imagen = $imagen;
		$this -> conexion = new Conexion();
		$this -> productoDAO = new productoDAO($this->id,$this->nombre,$this->precio,$this->descripcion, $this -> imagen);
	}
	
	public function traerProducto(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos());
		$productos = array();
		while(($registro = $this -> conexion -> extraer()) != null){
			$producto = new producto($registro[0], $registro[1], $registro[2], $registro[3],$registro[4]);
			array_push($productos, $producto);
		}
		$this -> conexion -> cerrar();
		return $productos;
	}

	public function traerProductoId(){
		$this -> conexion -> abrir();
		$this -> conexion -> ejecutar($this -> productoDAO -> consultarId());
		while(($registro = $this -> conexion -> extraer()) != null){
			$producto = new producto($registro[0], $registro[1], $registro[2], $registro[3],$registro[4]);
		}
		$this -> conexion -> cerrar();
		return $producto;
	}
	
	
	
	
}

?>
