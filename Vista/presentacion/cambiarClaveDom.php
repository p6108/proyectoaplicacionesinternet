 <?php
	include 'Vista/presentacion/menuDom.php';
	$idDom = $_GET["idDom"];
	// $Administrador = new cliente($idAdministrador);
	// $Administrador -> consultar();
	if (isset($_POST["editar"])) {
		$Dom = new dom($idDom, "", "", "", $_POST['ClaveNueva']);
		$Dom->CambiarClave();
	}
	?>
<script> 
 function validarClave(){
 var pas1 = document.getElementById('pas1');
 var pas2 = document.getElementById('pas2');
 var btn = document.getElementById('btn');
 if (pas1.value != pas2.value) {
    // Si las constraseñas no coinciden mostramos un mensaje
  btn.disabled=true;
}
else {
     btn.disabled=false;
}
 }
 </script>
<div class="container">
	<div class="row mt-3">
		<div class="col-xs-12 col-lg-4 text-center"></div>
		<div class="col-xs-12 col-lg-4 text-center">
			<div class="card">
				<h5 class="card-header bg-info text-white">Cambio de Contraseña</h5>
				<div class="card-body">	
				<?php if(isset($_POST["editar"])) { ?>
				<div class="alert alert-success alert-dismissible fade show"
						role="alert">
						Datos registrados correctamente
						<button type="button" class="btn-close" data-bs-dismiss="alert"
							aria-label="Close"></button>
					</div>
					<?php } ?>	
					  <form method="post"
						action="index.php?pid=<?php echo base64_encode("Vista/presentacion/cambiarClaveDom.php")?>&idDom=<?php echo $idDom?>">
						<div class="col-md-12">
							<label for="inputEmail4" class="form-label">Contraseña Nueva</label>
							<input id="pas1" type="password" class="form-control"
								name="ClaveNueva" placeholder="Contraseña Nueva"
								required="required">
						</div>
						<div class="col-md-12">
							<label for="inputEmail4" class="form-label">Confirme la nueva
								contraseña</label> <input id="pas2" onkeyup="validarClave()"
								type="password" class="form-control" name="ClaveConfirmada"
								placeholder="Confirmar Contraseña" required="required">
						</div>
						<br>
						<div class="col-12">
							<button id="btn" disabled type="submit" name="editar"
								class="btn btn-info">Cambiar Contraseña</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
