<?php
include 'Vista/presentacion/menuCliente.php';
require_once 'Controlador/logica/carrito.php';
require_once 'Controlador/logica/compra.php';
require_once 'fpdf/fpdf.php';
$cli = new cliente($_SESSION["id"]);
$idCli = $cli->getId();
$hostorial = new historial("","","","","",$idCli);
$histo = $hostorial -> verHistorial();
$car = new carrito("", $idCli);
$historiales =$hostorial->grafica2();
if (isset($_POST["PDF"])) {
	$pdf = new fpdf('L','mm', 'Letter');
	$pdf -> SetMargins(10, 10, 10);
	$pdf -> AddPage();
	$pdf -> Image("img/logo.png", 10, 10, 15, 15);
	$pdf -> SetFont('Times', 'B', 18);
	$pdf -> Cell(255, 15, "Reporte de las Compras Registrados", 0, 1, 'C');
	$pdf -> SetFont('Times', 'B', 10);
	$pdf -> Cell(10, 8, "", 0, 0, 'C');
	$pdf -> Cell(225, 	8, "Compras Registrados", 1, 1, 'C');
	$pdf -> Cell(10, 8, "", 0, 0, 'C');
	$pdf -> Cell(40, 8, "Id de la Compra", 1, 0, 'C');
	$pdf -> Cell(30, 8, "Fecha", 1, 0, 'C');
	$pdf -> Cell(60, 8, "Productos", 1, 0, 'C');
	$pdf -> Cell(20, 8, "Cantidad", 1, 0, 'C');
	$pdf -> Cell(25, 8, "precio", 1, 0, 'C');
	$pdf -> Cell(25, 8, "Subtotal", 1, 0, 'C');
	$pdf -> Cell(25, 8, "Total", 1, 1, 'C');
	$pdf -> SetFont('Times', '', 10);
	$cont =0;
	$totall = 0;
	$total1 = 0;
	$total2 = 0;
	foreach($histo as $var) {	
		
		/*$pdf -> Cell(40, 8, utf8_decode($clienteactual->getNombre()), 1, 0, 'C');
		$pdf -> Cell(40, 8, $clienteactual->getDireccion() , 1, 0, 'C');
		$pdf -> Cell(30, 8, $clienteactual->getTelefono(), 1, 0, 'C');
		$pdf -> Cell(40, 8, $clienteactual->getCorreo() , 1, 1, 'C');*/
		if($cont == 0){
			$varId = "";
			$pdf -> Cell(10, 8, "", 0, 0, 'C');
			$pdf -> Cell(40, 8, $var->getIdCompra(), 1, 0, 'C');
			$pdf -> Cell(30, 8, $var->getFecha(), 1, 0, 'C');
			$pdf -> Cell(60, 8, $var-> getNombreProducto(), 1, 0, 'C');
			$pdf -> Cell(20, 8,  $var-> getcantidad(), 1, 0, 'C');
			$pdf -> Cell(25, 8, $var-> getprecio(), 1, 0, 'C');
			$pdf -> Cell(25, 8,$var-> getsubtotal(), 1, 0, 'C');
			$total1 +=  $var-> getsubtotal();
			$pdf -> Cell(25, 8,$total1, 1, 1, 'C');
			$varId = $var->getIdCompra();
		
			
		}else if($varId == $var->getIdCompra() && $cont>0){
			$pdf -> Cell(80, 8, "", 0, 0, 'C');
			$pdf -> Cell(60, 8, $var-> getNombreProducto(), 1, 0, 'C');
			$pdf -> Cell(20, 8,  $var-> getcantidad(), 1, 0, 'C');
			$pdf -> Cell(25, 8, $var-> getprecio(), 1, 0, 'C');
			$pdf -> Cell(25, 8,$var-> getsubtotal(), 1, 0, 'C');
			$total2 +=  $var-> getsubtotal();
			$totall = $total1+$total2;
			$pdf -> Cell(25, 8,$totall, 1, 1, 'C');
			
			
		}else if($varId != $var->getIdCompra() && $cont >0){
			$total1 =0;
			$total2 =0;
			$pdf -> Cell(10, 8, "", 0, 0, 'C');
			$pdf -> Cell(40, 8, $var->getIdCompra(), 1, 0, 'C');
			$pdf -> Cell(30, 8, $var->getFecha(), 1, 0, 'C');
			$pdf -> Cell(60, 8, $var-> getNombreProducto(), 1, 0, 'C');
			$pdf -> Cell(20, 8,  $var-> getcantidad(), 1, 0, 'C');
			$pdf -> Cell(25, 8, $var-> getprecio(), 1, 0, 'C');
			$pdf -> Cell(25, 8,$var-> getsubtotal(), 1, 0, 'C');

			$total1 +=  $var-> getsubtotal();
			
			
			$totall = $total1+$total2;
			$pdf -> Cell(25, 8,$totall, 1, 1, 'C');
	
			$varId = $var->getIdCompra();	
		}
		$cont ++;
	}
	
	ob_end_clean();
	$pdf -> Output('I');
}
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Consulta de las compras</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>

                                <th scope="col" colspan="7" class="text-center table-warning">Compras Realizadas</th>

                            </tr>
                            <tr>
                                <th scope="col">Id de la compra</th>
                                <th scope="col">fecha</th>
                                <th scope="col">productos</th>
                                <th scope="col">cantidad</th>
                                <th scope="col">precio</th>
                                <th scope="col">subtotal</th>
                                <th scope="col">Total</th>

                            </tr>
                        </thead>
                        <tbody>
						<?php
							$cont =0;
							$totall = 0;
							$total1 = 0;
							$total2 = 0;
                            foreach($histo as $var) {		
								if($cont == 0){
									$varId = "";
									echo "<tr>";
									echo "<td>" . $var->getIdCompra() . "</td>";
									echo "<td>" . $var->getFecha() . "</td>";
									echo "<td>" . $var-> getNombreProducto() . "</td>";
									echo "<td>" . $var-> getcantidad() . "</td>";
									echo "<td>" . $var-> getprecio() . "</td>";
									echo "<td>" . $var-> getsubtotal() . "</td>";
									$total1 +=  $var-> getsubtotal();
									echo "<td> <b> " . $total1. "</td>";
									$varId = $var->getIdCompra();
								
								
								}else if($varId == $var->getIdCompra() && $cont>0){
									echo "<tr>";
									echo "<td></td>";
									echo "<td></td>";
									echo "<td>" . $var-> getNombreProducto() . "</td>";
									echo "<td>" . $var-> getcantidad() . "</td>";
									echo "<td>" . $var-> getprecio() . "</td>";
									echo "<td>" . $var-> getsubtotal() . "</td>";
									$total2 +=  $var-> getsubtotal();
									$totall = $total1+$total2;
								
									echo "<td> <b>" . $totall. "</td>";
								
							
								}else if($varId != $var->getIdCompra() && $cont >0){
									$total1 =0;
									$total2 =0;
									echo "<tr>";
									echo "<td>" . $var->getIdCompra() . "</td>";
									echo "<td>" . $var->getFecha() . "</td>";
									echo "<td>" . $var-> getNombreProducto() . "</td>";
									echo "<td>" . $var-> getcantidad() . "</td>";
									echo "<td>" . $var-> getprecio() . "</td>";
									echo "<td>" . $var-> getsubtotal() . "</td>";
									
									$total1 +=  $var-> getsubtotal();
								
								
									$totall = $total1+$total2;
									echo "<td> <b>" . $totall. "</td>";
									$varId = $var->getIdCompra();
								
								}
                            	$cont ++;
                            	
                            }
                            ?>
                            
                            
                        </tbody>
                    </table>
                    <form method="post"
                       	action="index.php?pid=<?php echo base64_encode("Vista/presentacion/consultarProductoCliente.php")?>">
							<div class="col-12">
							<button type="submit" name="PDF" class="btn btn-dark">Generar PDF</button>
							</div>
						</form>	
                </div>
            </div>
        </div>
    </div>
</div>

<div id="piechart" style="width: 900px; height: 500px;"> xD</div>
</div>

<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Producto', 'Cantidad'],
		  <?php 
		 	 foreach ($historiales as $hActual){
				echo "['" . $hActual[0] . "', " . $hActual[1] . "],";
			} 
		  ?>
          
        ]);

        var options = {
          title: 'sus productos más Comprados ',
		  is3D: true,

        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>


</script>
