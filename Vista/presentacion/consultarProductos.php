<?php
include 'Vista/presentacion/menuCliente.php';
$producto = new producto();
$productos = $producto->traerProducto();
/*include 'Controlador/logica/producto.php';
include'Vista/presentacion/carrito.php';*/
//include 'Controlador/logica/carrito.php';
$cli = new cliente($_SESSION["id"]);
$idCli = $cli->getId();
if(isset($_POST['btnActionAgregar'])){
	$c = new carrito("",$idCli, $_POST['id'], $_POST['cant']);
	//echo $_POST['cant'];
	$c ->agregarCarro();	
}


?>

<br>
<div class="container">
    <div class="row mt-3">
        <div class="col md-1 ">
            <div class="alert alert-success">

                <a class="logo" href="index.php?pid=<?php echo base64_encode("Vista/presentacion/verCarrito.php")?>"
                    class="badge badge-success">
                    <img src="img/carrito.jfif" alt="">
                </a>
            </div>
            <div class="card">
                <h5 class="card-header">Productos</h5>
                <div class="row row-cols-1 row-cols-md-3 g-4 mt-3 offset-md-1">

                    <?php
					foreach ($productos as $productoActual) {
						?>
                    <div class="row mt-3 justify-content-center align-items-center">
                        <div class="col-auto">
                            <div class="card" style="width: 18rem;">
                                <img title="<?php echo $productoActual -> getNombre() ?>"
                                    src="<?php echo $productoActual -> getImagen()?>" class="card-img-top"
                                    alt="<?php echo $productoActual -> getNombre()?>">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $productoActual -> getPrecio() ?>$</h5>
                                    <p class="card-text"><?php echo $productoActual -> getDescripcion()?></p>
                                    <form action="" method="post">                             
                                        <div class="quantity">
                                            <input type="number" min="1"  step="1" value="1" id="cant" name="cant" >
                                        </div>
                                        <input type="hidden" name="id" id="id"
                                            value=<?php echo $productoActual -> getId(); ?>>
                                        <input type="hidden" name="nombre" id="nombre"
                                            value=<?php echo $productoActual -> getNombre();?>>
                                        <input type="hidden" name="precio" id="precio"
                                            value=<?php echo $productoActual -> getPrecio();?>>
                                        <input type="hidden" name="cantidad" id="cantidad" value="">
                                        <input type="hidden" name="descripcion" id="descripcion"
                                            value=<?php echo $productoActual -> getDescripcion();?>>
                                        <input type="hidden" name="imagen" id="imagen"
                                            value=<?php echo $productoActual -> getImagen();?>>
										<br>
                                        <button class="btn btn-success" name="btnActionAgregar" value="Agregar"
                                            type="submit">Agregar al carrito</button>
                                    </form>


                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>

                </div>
            </div>
        </div>
    </div>
</div>