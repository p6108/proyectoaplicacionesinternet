 <?php
 include 'Vista/presentacion/menuCliente.php';
 $idCliente = $_GET["idCliente"];
 $cliente = new cliente($idCliente);
 $cliente -> consultar();
 if(isset($_POST["editar"])){
 	$cliente = new cliente($idCliente,$_POST['Nombre'],$_POST['Direccion'],$_POST['Telefono'],"","");
 $cliente -> CambiarDatos();
 }
 ?>

<div class="container">
	<div class="row mt-3">
		<div class="col-xs-12 col-lg-4 text-center"></div>
		<div class="col-xs-12 col-lg-4 text-center">
			<div class="card">
				<h5 class="card-header bg-success text-white">Registro de Datos Nuevos</h5>
				<div class="card-body">
					<?php if(isset($_POST["editar"])) { ?>
				<div class="alert alert-success alert-dismissible fade show"
						role="alert">
						Datos registrados correctamente
						<button type="button" class="btn-close" data-bs-dismiss="alert"
							aria-label="Close"></button>
					</div>
					<?php } ?>	
					  <form method="post"
						action="index.php?pid=<?php echo base64_encode("Vista/presentacion/editarperfilCliente.php")?>&idCliente=<?php echo $idCliente?>">
						<div class="col-md-12">
							<label for="inputEmail4" class="form-label">Nombre</label> <input
								type="text" class="form-control" name="Nombre"
								placeholder="Nombre" required="required">
						</div>

						<div class="col-md-12">
							<label for="inputEmail4" class="form-label">Telefono</label> <input
								type="number" class="form-control" name="Telefono"
								placeholder="Telefono" required="required">
						</div>

						<div class="col-md-12">
							<label for="inputEmail4" class="form-label">Dirección</label> <input
								type="text" class="form-control" name="Direccion"
								placeholder="Direccion" required="required">
						</div>
						<br>
						<div class="col-12">
							<button type="submit" name="editar" class="btn btn-success">Cambiar Datos</button>

						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


