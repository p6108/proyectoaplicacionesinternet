<?php
require_once 'Controlador/logica/Administrador.php';
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container bg-success p-2" style="--bs-bg-opacity: .5;" >
	<div class="row">
		<div class="col">
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container-fluid">
					<a class="navbar-brand"
						href="index.php?pid=<?php echo base64_encode("Vista/presentacion/sesionAdministrador.php") ?>"><i
						class="fas fa-home"></i></a>
					<button class="navbar-toggler" type="button"
						data-bs-toggle="collapse" data-bs-target="#navbarNav"
						aria-controls="navbarNav" aria-expanded="false"
						aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNav">
					<ul class="navbar-nav">
							<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
								href="#" id="navbarDropdown" role="button"
								data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrador: <?php echo $administrador -> getNombre() ?></a>
								<div class="dropdown-menu">
								<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Vista/presentacion/verCompras.php")?>">Ver compras realizadas</a> 
								<a class="dropdown-item"href="index.php?pid=<?php echo base64_encode("Vista/presentacion/reporteADMIN.php") ?>">Generar pdf de las compras</a>
								<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Vista/presentacion/verEmpleados.php")?>">Ver empleados</a> 
									<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Vista/presentacion/verRegistrosC.php")?>">Clientes registrados</a>
									<a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("Vista/presentacion/editarperfilAdministrador.php") ?>&idAdministrador=<?php echo $administrador -> getId()?>">Editar Perfil</a> 
									<a class="dropdown-item"href="index.php?pid=<?php echo base64_encode("Vista/presentacion/cambiarClaveAdministrador.php") ?>&idAdministrador=<?php echo $administrador -> getId()?>">Cambiar Clave</a>
									
								</div></li>
							<li class="nav-item"><a class="nav-link"
								href="index.php?sesion=false">Cerrar Sesion</a></li>
						</ul>
					</div>

				</div>
			</nav>
		</div>
	</div>
</div>






