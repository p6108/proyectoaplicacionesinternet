<?php
include 'Vista/presentacion/encabezado.php';
$his = new historial();
$historiales = $his->grafica();

?>
<br>
<div class="container">
	<div class="col-xs-9 col-sm-8 col-md-12 ">
		<h5 class="fs-1 ">
			<center>♦♠♣ ----------------- Tocayo's Pizza ----------------- ♣♠♦
		
		</h5>
		<br>
		<div id="carouselExampleFade" class="carousel slide carousel-fade "
			data-bs-ride="carousel">
			<div class="carousel-inner">
				<div class="carousel-item active">
					<img src="img/p1.jpg" width="200" height="450"
						class="d-block w-100" alt="...">
				</div>
				<div class="carousel-item">
					<img src="img/p2.jpg" width="200" height="450"
						class="d-block w-100" alt="...">
				</div>
				<div class="carousel-item">
					<img src="img/p3.jpg" width="200" height="450"
						class="d-block w-100" alt="...">
				</div>
			</div>
			<button class="carousel-control-prev" type="button"
				data-bs-target="#carouselExampleFade" data-bs-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Previous</span>
			</button>
			<button class="carousel-control-next" type="button"
				data-bs-target="#carouselExampleFade" data-bs-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="visually-hidden">Next</span>
			</button>
		</div>
	</div>
	<br>
	<div class="card">
		<h5 class="card-header">
			<center>Productos
		
		</h5>
		<br>
		<div class="row row-cols-1 row-cols-md-3 g-4">
			<div class="col">
				<div class="card">
					<img src="img/pizza_peppe_peque.jpg" class="card-img-top">
					<div class="card-body">
						<h5 class="card-title">
							<center>Porción de Pizza
						
						</h5>
						<p class="card-text">Obten una explosión de sabores en tu boca a
							provar un solo bocado de nuestras pizzas.</p>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<img src="img/pizza_hawa_Fa.jpg" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">
							<center>Pizza Familiar
						
						</h5>
						<p class="card-text">Una de nuestras pizzas te garantizan un
							excelente dia.</p>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card">
					<img src="img/gaseosa_postobon_350.jpg" class="card-img-top"
						alt="...">
					<div class="card-body">
						<h5 class="card-title">Gaseosas</h5>
						<p class="card-text">Acompaña tu pizza con una refrescante Bebida.</p>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="col">
			<div class="card" style="width: 100%;">
				<img src="img/jaja.jfif" class="card-img-top" alt="...">
			</div>
		</div>
	</div>
	<br>
	<div id="piechart" style="width: 900px; height: 500px;"></div>
	<div class="col">
		<div class="card text-center">
			<div class="card-header">Pagina realizada por</div>
			<div class="card-body">

				<h5 class="fst-italic">José Alejandro Rincón y José Antonio López</h5>

			</div>
			<div class="card-footer text-muted">@Copyright, 2022</div>
		</div>
	</div>

</div>
<br>
</div>

<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Producto', 'Cantidad'],
		  <?php
				foreach ($historiales as $hActual) {
					echo "['" . $hActual[0] . "', " . $hActual[1] . "],";
				}
				?>
          
        ]);

        var options = {
          title: 'Productos más populares'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>


</script>
