<?php
include 'Vista/presentacion/encabezado.php';
if (isset($_POST["registrar"])) {
	$cliente = new cliente(($_POST['idCliente']), ($_POST['Nombre']), ($_POST['Direccion']), ($_POST['Telefono']), ($_POST['Email']), ($_POST['Clave']));
	$cliente -> crear();
}
?>

<div class="container">
	<div class="row mt-3">
		<div class="col-xs-12 col-lg-4 text-center"></div>
		<div class="col-xs-12 col-lg-4 text-center">
			<div class="card">
				<h5 class="card-header bg-primary text-white">Registro</h5>
				<div class="card-body">
				<?php if(isset($_POST["registrar"])) { ?>
				<div class="alert alert-success alert-dismissible fade show"
						role="alert">
						Datos registrados correctamente
						<button type="button" class="btn-close" data-bs-dismiss="alert"
							aria-label="Close"></button>
					</div>
					<?php } ?>	
               <form method="post"
						action="index.php?pid=<?php echo base64_encode("Vista/presentacion/registropersonas.php")?>"
						class="row g-3">
						<div class="col-md-12">

							<label for="exampleInputEmail1" class="form-label">Numero de
								Documento</label> <input type="number" class="form-control"
								name="idCliente" required="required" placeholder="No Documento">
						</div>

						<div class="col-md-6">
							<label for="inputEmail4" class="form-label">Correo</label> <input
								type="email" class="form-control" name="Email"
								placeholder="Email" required="required">
						</div>
						<div class="col-md-6">
							<label for="inputEmail4" class="form-label">Clave</label> <input
								type="password" class="form-control" name="Clave"
								placeholder="Clave" required="required">
						</div>
						<div class="col-md-6">
							<label for="inputEmail4" class="form-label">Nombre</label> <input
								type="text" class="form-control" name="Nombre"
								placeholder="Nombre" required="required">
						</div>
						<div class="col-md-6">
							<label for="inputEmail4" class="form-label">Telefono</label> <input
								type="number" class="form-control" name="Telefono"
								placeholder="Telefono" required="required">
						</div>

						<div class="col-md-12">
							<label for="inputEmail4" class="form-label">Dirección</label> <input
								type="text" class="form-control" name="Direccion"
								placeholder="Direccion" required="required">
						</div>
						<div class="col-12">
							<button type="submit" name="registrar" class="btn btn-primary">Registrar</button>

						</div>
				  </form>
				</div>
			</div>
		</div>
	</div>
</div>



