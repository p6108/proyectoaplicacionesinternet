<?php
require_once 'Controlador/logica/historial.php';
require_once 'fpdf/fpdf.php';

$historial = new historial();
$arrayHistorial = $historial ->verHistorial2();

$pdf = new fpdf('P','mm', 'Letter');
$pdf -> SetMargins(10, 10, 10);
$pdf -> AddPage();
$pdf -> Image("img/logo.png", 10, 10, 15, 15);
$pdf -> SetFont('Times', 'B', 18);
//$pdf -> Text(20, 20, 'Hola Mundo');
$pdf -> Cell(196, 15, "Reporte", 0, 1, 'C');

$pdf -> SetFont('Times', 'B', 10);

$pdf -> Cell(10, 8, "", 0, 0, 'C');
$pdf -> Cell(175, 8, "Compras Realizadas", 1, 1, 'C');

$pdf -> Cell(10, 8, "", 0, 0, 'C');
$pdf -> Cell(40, 8, "Fecha de la compra", 1, 0, 'C');
$pdf -> Cell(40, 8, "Id de la Compra", 1, 0, 'C');

$pdf -> Cell(30, 8, "Id del Cliente", 1, 0, 'C');

$pdf -> Cell(35, 8, "Total", 1, 0, 'C');
$pdf -> Cell(30, 8, "Id Empleado", 1, 1, 'C');
$pdf -> SetFont('Times', '', 10);

$ids = array();

foreach ($arrayHistorial as $ha) {
	$idCompra = $ha->getIdCompra();
	array_push($ids,$idCompra);
	
}

array_unshift($ids,0);
for($x=1;$x<count($ids);$x++){
	if($ids[$x]!= $ids[$x-1]){
		$fila = array();
		$fila = ($historial ->verHistorial3($ids[$x]));
		echo "<tr>";
		for($y=0;$y<count($fila);$y++){
			if($y== 1){
				$pdf -> Cell(10, 8, "", 0, 0, 'C');
				$pdf -> Cell(40, 8, $fila[$y],1,0, 'c');
			}else if($y== 4){
				$pdf ->Cell(40, 8, $fila[$y], 1, 0, 'C');
			}else if($y==5){
				$pdf ->Cell(30, 8, $fila[$y], 1, 0, 'C');
			}else if($y==9){
				$pdf ->Cell(35, 8, $fila[$y], 1, 0, 'C');
			}else if($y==10){
				$pdf ->Cell(30, 8, $fila[$y], 1, 1, 'C');
			}
			
		}
		
		
	}
	
}

ob_end_clean();
$pdf -> Output('I');
?>

    
?>
