<?php
require_once 'Controlador/logica/Administrador.php';
include 'Vista/presentacion/menuAdministrador.php';
$administrador = new Administrador($_SESSION["id"]);
$administrador -> consultar();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header"><?php echo $administrador -> getNombre(); ?></h5>
				<div class="card-body">
			
					<div class="container   text-dark">
						<?php echo "Correo: " . $administrador -> getCorreo(); ?> <br>
						<br>
						<div class="card " style="width: 14rem;">
							<img src="img/perfil.jpg" class="card-img-top" alt="...">
								<h6 class="card-title fw-bold"><?php echo $administrador->getNombre()?>Administrador</h6>
								<p class="card-text fst-italic">Ingeniero en sistemas,
									emprendedor y administrador de Tocayo´s pizza</p>
							
						</div>


					</div>
				</div>
			</div>
		</div>
	</div>
</div>
