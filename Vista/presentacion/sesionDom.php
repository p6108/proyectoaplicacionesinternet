<?php
require_once 'Controlador/logica/dom.php';
include 'Vista/presentacion/menuDOM.php';

$dom = new dom($_SESSION["id"]);
$dom -> consultar();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header"><?php echo $dom -> getNombre(); ?></h5>
				<div class="card-body">

					<div class="container   text-dark">
						<?php echo "Correo: " . $dom -> getCorreo(); ?> <br> <br>
						<div class="card " style="width: 14rem;">
							<img src="img/dom.jpg" class="card-img-top" alt="...">
							<div class="card-body">
								<h6 class="card-title fw-bold"><?php echo $dom -> getNombre()?></h6>
								<p class="card-text fst-italic"></p>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
