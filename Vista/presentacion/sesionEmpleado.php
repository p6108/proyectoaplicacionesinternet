<?php
require_once 'Controlador/logica/empleado.php';
include 'Vista/presentacion/menuEmpleado.php';

$empleado = new empleado($_SESSION["id"]);
$empleado->consultar();
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header"><?php echo $empleado -> getNombre() ?></h5>
				<div class="card-body">
					<div class="container   text-dark">
						<?php echo "Correo: " .$empleado -> getCorreo(); ?> <br>
						<div class="card " style="width: 18rem;">
							<img src="img/empleado.jfif" class="card-img-top" alt="...">
							<div class="card-body">
								<h5 class="card-title fw-bold"><?php echo $empleado->getNombre();?> </h5>
								<p class="card-text fst-italic"></p>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>

