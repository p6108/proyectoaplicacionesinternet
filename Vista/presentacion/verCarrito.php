<?php
require_once 'Controlador/logica/carrito.php';
require_once 'Controlador/logica/pedido.php';
include 'Vista/presentacion/menuCliente.php';
require_once 'Controlador/logica/compra.php';
require_once 'fpdf/fpdf.php';
$cli = new cliente($_SESSION["id"]);
$idCli = $cli->getId();
$c = new carrito("", $idCli);
$compra = new compra();
$idc = $compra->getIdCompra();
$car = new carrito("", $idCli);
$cantidades = $car ->  traerCant();//trae cantidades de los productos
$idEmpleado = rand(1,3);
$idP = $car->traerProducto();//array productos
$objPedido = new pedido();
$idrandom = rand(1000000, 9999999);
$infoo = $car->verCarrito();
if (isset($_POST['namebtncompra'])) {
	$com = new compra($idrandom, "", $idCli);
	$com->agregarCompra();
	$objPedido = new pedido("", $idrandom, $idCli, $idEmpleado);
	$objPedido->insertarpedido();
	
	//echo '<script language="javascript">alert("Compra terminada... Reclame su pedido!!");</script>';
	$pdf = new fpdf('L','mm', array(90,280));
	$pdf -> SetMargins(10, 10, 10);
	$pdf -> AddPage();
	$pdf -> Image("img/logo.png", 10, 10, 15, 15);
	$pdf -> SetFont('Times', 'B', 18);
	$pdf -> Cell(250, 15, "Factura", 0, 1, 'C');
	$pdf -> SetFont('Times', 'B', 10);
	$pdf -> Cell(10, 8, "", 0, 0, 'C');
	$pdf -> Cell(230, 	8, "Compra Registrada", 1, 1, 'C');
	$pdf -> Cell(10, 8, "", 0, 0, 'C');
	$pdf -> Cell(40, 8, "Nombre del producto", 0, 0, 'C');
	$pdf -> Cell(140, 8, utf8_decode("Descripción"), 0, 0, 'C');
	$pdf -> Cell(30, 8, "Precio Unitario", 0, 0, 'C');
	$pdf -> Cell(20, 8, "Cantidad", 0, 1, 'C');

	$pdf -> SetFont('Times', '', 10);
	
	$i = 0;
	$idpp = 0;

	foreach ($infoo as $var) {
		$pdf -> Cell(10, 8, "", 0, 0, 'C');
		$pdf -> Cell(40, 8, utf8_decode($var->getNombre()), 0, 0, 'C');
		$pdf -> Cell(140, 8, utf8_decode($var->getDescripcion()), 0, 0, 'C');
		$pdf -> Cell(30, 8,   $var->getPrecio(), 0, 0, 'C');
		$pdf -> Cell(20, 8,   $cantidades[$idpp], 0, 1, 'C');
		$i += $var->getPrecio() * $cantidades[$idpp];
		$idpp = $idpp +1;
	}
	$pdf -> Cell(10, 8, "", 0, 0, 'C');
	$pdf -> Cell(25, 8, "TOTAL : ", 0, 0, 'C');
	$pdf -> Cell(25, 8,  $i, 0	, 0, 'C');			  
	ob_end_clean();
	$pdf -> Output('I');
	$objPedido->borrarCarrito();
}
$cont = 0;
$total =0;
foreach ($idP as $idPP) {
	$idpp = $idPP->getId();
	$nombre = $idPP->getNombre();
	$precio = $idPP ->getPrecio();
	$subtotal = $precio * $cantidades[$cont];
	$total += $subtotal;
	$historial = new historial("", "", $idpp, $nombre, $idrandom, $idCli, $cantidades[$cont],$precio, $subtotal, $total,$idEmpleado);
	$historial->agregarHistorial();
	$cont ++;
}

$info = $car->verCarrito();
if (isset($_POST['namebtncompra'])) {
	
}
?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consulta del Carrito</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>

								<th scope="col" colspan="4" class="text-center table-warning">Producto</th>

							</tr>
							<tr>
								<th scope="col">Nombre Producto</th>
								<th scope="col">Descripcion</th>
								<th scope="col">precio Unitario</th>
								<th scope="col">cantidad</th>
							</tr>
						</thead>
						<tbody>
                            <?php
                            $i = 0;
                         
                            $idpp = 0;
                            foreach ($info as $var) {
                            	echo "<tr>";
                            	
                            	echo "<td>" . $var->getNombre() . "</td>";
                            	echo "<td>" . $var->getDescripcion() . "</td>";
                            	echo "<td>" . $var->getPrecio() . "</td>";
                            	echo "<td>" . $cantidades[$idpp] . "</td>";
                     
                            	$i += $var->getPrecio() * $cantidades[$idpp];
                    
                            	
                           		$idpp = $idpp +1;
                            	echo "</tr>";
                            	
                            	// echo "<td>" . $i . "</td>";
                            }
                            ?>
							<tr>
								<td colspan="2">TOTAL</td>
								<td> <?php echo $i;?></td>
							</tr>
						</tbody>
					</table>

					<form action="index.php?pid=<?php echo base64_encode("Vista/presentacion/verCarrito.php")?>" method="post">
						<button class="btn btn-success" name="namebtncompra"
							value="Agregar" type="submit">TERMINAR COMPRA</button>

					</form>

				</div>

			</div>

		</div>
	</div>
</div>
