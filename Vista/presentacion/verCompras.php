<?php
include 'Vista/presentacion/menuAdministrador.php';
require_once 'Controlador/logica/historial.php';
//$idAdministrador = $_GET["idAdministrador"]; 
/*$compra = new compra();
$compras = $compra -> ConsultarCompras();*/

$historial = new historial();
$arrayHistorial = $historial ->verHistorial2();
$h2 = $historial ->grafica();

?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Consultar compras</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>

                                <th scope="col" colspan="16" class="text-center table-warning">Compras Realizadas</th>

                            </tr>
                            <tr>
                                <th scope="col">Fecha de la compra</th>
                                <th scope="col">Id de la Compra</th>
                                <th scope="col">Id del Cliente</th>
                                <th scope="col">Total</th>
                                <th scope="col">Empleado</th>
								<!--<th scope="col">xD</th>-->

                            </tr>
                        </thead>
                        <tbody>
                            <?php
						$ids = array();
						?><?php

						foreach ($arrayHistorial as $ha) {
								$idCompra = $ha->getIdCompra();
								array_push($ids,$idCompra);
													
						}
						array_unshift($ids,0);
						for($x=1;$x<count($ids);$x++){
							if($ids[$x]!= $ids[$x-1]){
								$fila = array();
								$fila = ($historial ->verHistorial3($ids[$x]));
								echo "<tr>";
								for($y=0;$y<count($fila);$y++){
									if($y== 1 || $y== 4 || $y== 5 || $y== 9 || $y== 10){
										echo "<td>" . $fila[$y] ."</td>";
									}
									
								}
								//echo "<td>" . "xD" ."</td>";
								echo "</tr>";
							}
							//echo "<td>" . "xD" ."</td>";	?><!--<button type="button" class="btn btn-success">Success</button> --><?php
						}

						
						?>

                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>
<div id="chart_div" style="width: 900px; height: 500px;">:v</div>
</div>


<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

var data = new google.visualization.DataTable();
data.addColumn('string', 'Pizza');
data.addColumn('number', 'Populartiy');
data.addRows([
    <?php 
		 	 foreach ($h2 as $hActual){
				echo "['" . $hActual[0] . "', " . $hActual[1] . "],";
			} 
		  ?>
  /*['Pepperoni', 33],
  ['Hawaiian', 26],
  ['Mushroom', 22],
  ['Sausage', 10], // Below limit.
  ['Anchovies', 9]*/ // Below limit.
]);

var options = {
  title: 'productos que más vende',
 // sliceVisibilityThreshold: .2
  is3D: true,
};

var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
chart.draw(data, options);

}
    </script>


</script>
