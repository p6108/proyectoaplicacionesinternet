<?php
include 'Vista/presentacion/menuEmpleado.php';
require_once 'Controlador/logica/compra.php';
$empleado = new empleado($_SESSION["id"]);
$idEmpleado = $empleado->getId();
//
$hostorial = new historial("","","","","","","","","","",$idEmpleado);
$histo = $hostorial -> verHistorialEmpleado();

?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Consultar compras</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" colspan="5" class="text-center table-warning">Compras Realizadas</th>
                            </tr>
                            <tr>
                                <th scope="col">Id de la Compra</th>
                                <th scope="col">Fecha de la compra</th>
                                <th scope="col">Id del Cliente</th>
                                 <th scope="col">productos</th>
                                <th scope="col">cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $cont=0;
                            foreach($histo as $var) {
                            	if($cont == 0){
                            		$varId = "";
                            		echo "<tr>";
                            		echo "<td>" . $var->getIdCompra() . "</td>";
                            		echo "<td>" . $var->getFecha() . "</td>";
                            		echo "<td>" . $var->getIdCliente() . "</td>";
                            		echo "<td>" . $var-> getNombreProducto() . "</td>";
                            		echo "<td>" . $var-> getcantidad() . "</td>";
                            		$varId = $var->getIdCompra();
                            		
                            	}else if($varId == $var->getIdCompra() && $cont>0){
                            		echo "<tr>";
                            		echo "<td></td>";
                            		echo "<td></td>";
                            		echo "<td></td>";
                            		echo "<td>" . $var-> getNombreProducto() . "</td>";
                            		echo "<td>" . $var-> getcantidad() . "</td>";
                            		
                            	}else if($varId != $var->getIdCompra() && $cont >0){
                            		$total1 =0;
                            		$total2 =0;
                            		echo "<tr>";
                            		echo "<td>" . $var->getIdCompra() . "</td>";
                            		echo "<td>" . $var->getFecha() . "</td>";
                            		echo "<td>" . $var->getIdCliente() . "</td>";
                            		echo "<td>" . $var-> getNombreProducto() . "</td>";
                            		echo "<td>" . $var-> getcantidad() . "</td>";
                            		$varId = $var->getIdCompra();
                            	}
                            	$cont ++;
                            	
                            }
							?>

                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</div>