<?php
if($_SESSION["rol"] == "dom"){
include 'Vista/presentacion/menuDom.php';
require_once 'Controlador/logica/compra.php';
require_once 'Controlador/logica/cliente.php';
$compras = new compra();
$comprastotales = $compras -> ConsultarCompras(); 
$idC=0;

?>
<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar compras</h5>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th scope="col" colspan="8" class="text-center table-warning">Compras
									Realizadas</th>
							</tr>
							<tr>
								<th scope="col">Id de la Compra</th>
								<th scope="col">Fecha de la compra</th>
								<th scope="col">Id del Cliente</th>
								<th scope="col">Nombre del Cliente</th>
								<th scope="col">Dirección del Cliente</th>
								<th scope="col">Telefono del Cliente</th>
								<th scope="col">Estado del Domicilio</th>
								<th scope="col">Cambiar estado del Domicilio</th>
							</tr>
						</thead>
						<tbody>
                            <?php
                            
                            foreach($comprastotales as $var) {
                            	//$idC= $var -> getIdCompra();
                            	echo "<tr>";
                            	echo "<td>" . $var->getIdCompra() . "</td>";
                            	echo "<td>" . $var->getDate() . "</td>";
                            	echo "<td>" . $var->getIdClienteCom() . "</td>";
                            	$cliente = new cliente($var->getIdClienteCom());
                            	$infoc = $cliente ->consultar2();
                            	for($i=0;$i<count($infoc);$i++){
                            		echo "<td>" . $infoc[$i] . "</td>";
                            	}
                            	echo "<td> " . (( $var->getEstado()==1)?"<i id='iconoHabilitar" .  $var->getIdCompra() . "' class='fas fa-check-circle' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Entregado'></i>":"<i id='iconoHabilitar" .  $var->getIdCompra() . "' class='fas fa-times-circle' data-bs-toggle='tooltip' data-bs-placement='bottom' title='No entregado'></i>") . "</td>";
                            	echo "<td><a href='#'>" . (($var->getEstado()==1)?"<i id='botonHabilitar" .$var->getIdCompra() . "' class='fas fa-user-times' data-bs-toggle='tooltip' data-bs-placement='bottom' title='cancelar pedido'></i>":"<i id='botonHabilitar" .$var->getIdCompra(). "' class='fas fa-user-check' data-bs-toggle='tooltip' data-bs-placement='bottom' title='Completar domicilio'></i>") . "</a></td>";
                            
                            	?>

                            	<?php
                            }              
                            ?>
                            		</tbody>

					</table>

		<div id="resultados"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
    foreach($comprastotales as $varr) {
   $idC= $varr -> getIdCompra();
?>
<script>
$( '#botonHabilitar<?= $idC ?>' ).click(function() {	    
	if($( '#botonHabilitar<?= $idC ?>' ).attr('class') == 'fas fa-user-times'){	//si está en 1 la BD
		$( '#iconoHabilitar<?= $idC ?>' ).attr('class', 'fas fa-times-circle');	
		$('#iconoHabilitar<?= $idC ?>').attr('title', 'No Entregado');
		$( '#botonHabilitar<?= $idC ?>').attr('class', 'fas fa-user-check');
		$( '#botonHabilitar<?= $idC ?>').attr('title', 'Cancelar pedido');
		url = "indexAjax.php?pid=<?php echo base64_encode("Vista/presentacion/editarEstadoCompraAjax.php")?>&id=<?= $idC ?>&estado=0";
	}else{						//si está en 0 la BD
		$( '#iconoHabilitar<?= $idC ?>' ).attr('class', 'fas fa-check-circle');
		$( '#iconoHabilitar<?= $idC ?>').attr('title', 'Entregado');
		$( '#botonHabilitar<?= $idC ?>' ).attr('class', 'fas fa-user-times');
		$( '#botonHabilitar<?= $idC ?>').attr('title', 'Cancelar pedido');
		url = "indexAjax.php?pid=<?php echo base64_encode("Vista/presentacion/editarEstadoCompraAjax.php")?>&id=<?= $idC ?>&estado=1";
	}	
    $( "#resultados" ).load(url);    	
});  
	
</script>
<?php }	?>
<?php } else { ?>
<h1>Usted no se autenticó con el rol Domiciliario. No puede entrar</h1>
<?php } ?>