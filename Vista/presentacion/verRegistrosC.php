<?php
require_once 'Controlador/logica/compra.php';
require_once 'Controlador/logica/pedido.php';
require_once 'Controlador/logica/cliente.php';
include 'Vista/presentacion/menuAdministrador.php';
require_once 'fpdf/fpdf.php';
$cliente = new cliente();
$clientes = $cliente->consultarTodo();
if (isset($_POST["PDF"])) {
$pdf = new fpdf('P','mm', 'Letter');
$pdf -> SetMargins(10, 10, 10);
$pdf -> AddPage();
$pdf -> Image("img/logo.png", 10, 10, 15, 15);
$pdf -> SetFont('Times', 'B', 18);
$pdf -> Cell(196, 15, "Reporte de los Clientes Registrados", 0, 1, 'C');
$pdf -> SetFont('Times', 'B', 10);
$pdf -> Cell(22, 8, "", 0, 0, 'C');
$pdf -> Cell(150, 8, "Clientes Registrados", 1, 1, 'C');
$pdf -> Cell(22, 8, "", 0, 0, 'C');
$pdf -> Cell(40, 8, "Nombre", 1, 0, 'C');
$pdf -> Cell(40, 8, utf8_decode("Dirección"), 1, 0, 'C');
$pdf -> Cell(30, 8, "Telefono", 1, 0, 'C');
$pdf -> Cell(40, 8, "Correo", 1, 1, 'C');
$pdf -> SetFont('Times', '', 10);
foreach ($clientes as $clienteactual) {
	$pdf -> Cell(22, 8, "", 0, 0, 'C');
	$pdf -> Cell(40, 8, utf8_decode($clienteactual->getNombre()), 1, 0, 'C');
	$pdf -> Cell(40, 8, $clienteactual->getDireccion() , 1, 0, 'C');
	$pdf -> Cell(30, 8, $clienteactual->getTelefono(), 1, 0, 'C');
	$pdf -> Cell(40, 8, $clienteactual->getCorreo() , 1, 1, 'C');
}

ob_end_clean();
$pdf -> Output('I');
}
?>
<div class="container">
    <div class="row mt-3">
        <div class="col">
            <div class="card">
                <h5 class="card-header">Clientes</h5>
                <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>

                                <th scope="col" colspan="4" class="text-center table-warning">Pedidos</th>

                            </tr>
                            <tr>
                                <th scope="col">Nombre</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Telefono</th>
                                <th scope="col">Correo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
						foreach ($clientes as $clienteactual) {
							echo "<tr>";
							echo "<td>" . $clienteactual->getNombre() . "</td>";
							echo "<td>" . $clienteactual->getDireccion() . "</td>";
							echo "<td>" . $clienteactual->getTelefono() . "</td>";
							echo "<td>" . $clienteactual->getCorreo() . "</td>";
							echo "</tr>";
						}
						?>

 

                    </table>
                       </tbody>
                       <form method="post"
                       	action="index.php?pid=<?php echo base64_encode("Vista/presentacion/verRegistrosC.php")?>">
							<div class="col-12">
							<button type="submit" name="PDF" class="btn btn-dark">Generar PDF</button>
							</div>
						</form>	
                </div>
            </div>
        </div>
    </div>
</div>