	<?php 
	session_start();

	require_once 'Controlador/logica/Administrador.php';
	require_once 'Controlador/logica/cliente.php';
	require_once 'Controlador/logica/producto.php';
	require_once 'Controlador/logica/empleado.php';
	require_once 'Controlador/logica/carrito.php';
	require_once 'Controlador/logica/compra.php';
	require_once 'Controlador/logica/pedido.php';
	require_once 'Controlador/logica/historial.php';
	require_once 'Controlador/logica/dom.php';

	/*$producto = new productoDAO();
	$r = $producto ->traerdatos();
	$r2 = mysql_fetch_array($r);*/
	$pid = "";
	if(isset($_GET["sesion"]) && $_GET["sesion"] == "false"){
		$_SESSION["id"] = "";
		$_SESSION["rol"] = "";
	}
		if (isset($_GET["pid"])) {
			$pid = base64_decode($_GET["pid"]);
		}
		$paginasSinSesion = array(
			"Vista/presentacion/inicio.php",
			"Vista/presentacion/autenticar.php",
			
			
			
		)			
?>
<!DOCTYPE html>
<html lang="en">
<head>	
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Tienda</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" ></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" ></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})
</script>
</html>
<?php 
// xd   
if ($pid != "") {
	if(in_array($pid, $paginasSinSesion)){
		include $pid;
	}else{
		if(isset($_SESSION["id"]) && $_SESSION["id"] != ""){
			include $pid;
		}else{
			include 'Vista/presentacion/registropersonas.php';
		}
	}
} else {
	include 'Vista/presentacion/pagprincipal.php';
}
?>
</html>

