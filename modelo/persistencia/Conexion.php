	<?php
class Conexion{
	private $mysqli;
	private $resultado;
	
	public function abrir(){
		$this -> mysqli = new mysqli("localhost", "root", "", "mydb");
		$this -> mysqli -> set_charset("utf8");

	}
	//
	public function cerrar(){
		$this -> mysqli -> close();
	}
	
	public function ejecutar($sentencia){
		//echo "entró a ejecutar";
		$this -> resultado = $this -> mysqli -> query($sentencia);
		//echo "ejecutar dice: ".$sentencia."";
	}

	
	
	public function extraer(){
		return $this -> resultado -> fetch_row();
	}
	public function extraer2(){
		return $this -> resultado -> fetch_assoc();
	}
	
	public function numFilas(){
		return ($this -> resultado != null) ? $this -> resultado -> num_rows : 0;
	}
}
?>
