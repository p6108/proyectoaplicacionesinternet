<?php

class historialDAO{
    private $id;
	private $fecha;
	private $idProducto;
	private $NombreProducto;
	private $idCompra;
	private $idCliente;
    private $conexion;
	private $cantidad;
	private $precio;
	private $subtotal;
	private $total;
	private $idEmpleado;
	
	
	
	/**
	 * @return mixed
	 */
	public function getIdEmpleado()
	{
		return $this->idEmpleado;
	}

	/**
	 * @return mixed
	 */
	public function getTotal()
	{
		return $this->total;
	}

	public function getprecio()
	{
		return $this->precio;
	}

	public function getsubtotal()
	{
		return $this->subtotal;
	}

	public function getcantidad()
	{
		return $this->cantidad;
	}
	
	public function getNombreProducto()
	{
		return $this->NombreProducto;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getIdCliente()
	{
		return $this->idCliente;
	}

	public function getIdProducto()
	{
		return $this->idProducto;
	}

    public function getFecha()
	{
		return $this->fecha;
	}

    public function getIdCompra()
	{
		return $this->idCompra;
	}


	public function __construct($id, $fecha,$idProducto,$NombreProducto, $idCompra,$idCliente, $cantidad, $precio, $subtotal,$total,$idEmpleado)
	{
		//echo "Entró al contructor".$id."".$idProducto."".$idCliente."";

		$this -> id = $id;
		$this -> idCliente = $idCliente;
		$this -> idProducto = $idProducto;
		$this -> NombreProducto = $NombreProducto;
		$this -> fecha = $fecha;
        $this -> idCompra = $idCompra;
		$this -> cantidad = $cantidad;
		$this -> precio = $precio;
		$this -> subtotal = $subtotal;
		$this -> total = $total;
		$this -> idEmpleado = $idEmpleado;
		$this -> conexion = new Conexion();
	//	$this -> carritoDAO = new carritoDAO($this ->idCliente, $this ->idProducto);
	}

    public function crearHistorial(){
        //isset($_POST['btnAction']);
        return "INSERT INTO `historial`(`idHistorial`, `fecha`, `Producto_idProducto`,`NombreProducto`, `Compra_idCompra`, `Compra_Cliente_idCliente`, `cantidad` , `precio`, `subtotal`,`total`,`idEmpleado`)
		 VALUES (NULL, now(), '".$this->idProducto."','".$this->NombreProducto."','".$this->idCompra."', '".$this->idCliente."','".$this->cantidad."', '".$this->precio."', '".$this->subtotal."', '".$this->total."','".$this->idEmpleado."')";
    }

    

  public function verHistorial(){
    return "SELECT DISTINCT idHistorial, fecha, Producto_idProducto,NombreProducto, Compra_idCompra, Compra_Cliente_idCliente, cantidad, precio, subtotal, total, idEmpleado FROM historial
	 WHERE Compra_Cliente_idCliente = '".$this ->idCliente."'";
  }
  
  public function verHistorialEmpleado(){
  	return "SELECT  idHistorial, fecha, Producto_idProducto,NombreProducto, Compra_idCompra, Compra_Cliente_idCliente, cantidad, precio, subtotal, total, idEmpleado FROM historial
	 WHERE idEmpleado = '".$this ->idEmpleado."'";
  }
  
  public function traerP(){
  	return "SELECT DISTINCT Nombre from producto WHERE idProducto = '".$this ->idProducto."' ";
  }

  public function FunctionName()
  {
	return "SELECT idHistorial, fecha, NombreProducto, Compra_idCompra, cantidad FROM historial WHERE idEmpleado = ";
  }
  
  public function verHistorial2(){
    return "SELECT DISTINCT idHistorial, fecha, Producto_idProducto,NombreProducto, Compra_idCompra, Compra_Cliente_idCliente, cantidad, precio, subtotal,total, idEmpleado FROM historial ";
  }

  public function traerMax($id){
	return "select DISTINCT MAX(total) from historial where Compra_idCompra = '".$id."'";
  }

  public function datos($max){
	return "SELECT * FROM historial WHERE total = '".$max."'";
  }

  public function verHistorial3($id){
    return "select idHistorial, fecha, Producto_idProducto,NombreProducto, Compra_idCompra, Compra_Cliente_idCliente, cantidad, precio, subtotal, MAX(total), idEmpleado from historial where Compra_idCompra = '".$id."'";
  }
  public function grafica(){
  	return "select NombreProducto, cantidad FROM historial";
  }
  public function grafica2(){
  	return "select NombreProducto, cantidad FROM historial where Compra_Cliente_idCliente ='".$this->idCliente."' ";
  }
  
}


?>
